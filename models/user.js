import mongoose from 'mongoose'
const Schema = mongoose.Schema

const userSchema = Schema ({
    username: {type: String, index:{unique:true}},
    name: String,
    email: String,
    password:String,
    image: String,
    country: String,
    city: String,
    rep: Number, //Reputación
    bday: String, //Fecha de nacimiento
    bio: String, //Biografía
    date_added: Date,
    role: Number,
    level: Number,
    wallet: Number
})


export default mongoose.model('User', userSchema)