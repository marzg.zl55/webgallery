import mongoose from 'mongoose'

const Schema = mongoose.Schema

const productSchema = Schema ({
    productid: {type: Number, index:{unique:true}},
    title: String,
    description: String,
    valoration: Number,
    username: String,
    price:Number,
    images: Array,
    tags: Array,
    date_added: Date,
    state: Number
})

export default mongoose.model('Product', productSchema)