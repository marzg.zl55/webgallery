import mongoose from 'mongoose'
const Schema = mongoose.Schema

const transactionSchema = Schema ({
    id: Number,
    productid: Number,
    buyer: String,
    total: Number,
    date: Date,
    fee: Number,
    payment_state:Number,
    shipping_state: Number,
    shipping_code: String,
    shipping_company: String
})

transactionSchema.index({id:1})

export default mongoose.model('Transaction', transactionSchema)