import mongoose from 'mongoose'
const Schema = mongoose.Schema

const reviewSchema = Schema ({
    id: {type: Number, index:{unique:true}},
    username: String,
    productid: Number,
    valoration: Number,
    title: String,
    body: String
})

export default mongoose.model('Review', reviewSchema)