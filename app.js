import createError from 'http-errors'
import express from 'express'
import path from 'path'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import indexRouter from './routes/index.js'
import usersRouter from './routes/users.js'
import productsRouter from './routes/products.js'
import adminRouter from './routes/admin.js'
import authRouter from './routes/auth.js'
import cors from 'cors'
import bodyParser from 'body-parser'
import authController from './controllers/auth.js'
import transactionRouter from './routes/transaction.js'

import db from './models/db.js'
var app = express();
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: true , limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(cors({}));
app.use(bodyParser.json({limit: '50mb'}));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use('/', indexRouter)
app.use('/user', usersRouter)
app.use('/products', productsRouter)
app.use('/admin', [authController.verifyToken], adminRouter)
app.use('/auth', authRouter)
app.use('/transactions', transactionRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

db.connect()

export default app
