# WEBGALLERY - API

Esta api proporciona las operaciones básicas para el manejo de datos de la app Webgallery

## Instalación

Clona el contenido del repositorio

```bash
git clone https://gitlab.com/marzg.zl55/webgallery.git
```
Instala las dependencias

```bash
npm install
```

## Ejecución

```bash
npm run start
```
-----
# API

## Usuarios

El punto de acceso a la api de usuarios es:

```bash
localhost:3000/user
```

### &nbsp; Obtener usuarios

- Para obtener una lista de todos los usuarios, enviar una petición GET con el siguiente cuerpo: 
`{}`
- Para obtener un usuario por alguna de sus propiedades, enviar una petición GET con la propiedad a filtrar y su valor en el cuerpo. 
Ej: si se quiere encontrar un usuario con el correo "van.gogh@gmail.com", el cuerpo de la petición será `{"email":"van.gogh@gmail.com"}`. Funciona para las siguientes propiedades de usuario: `username` `email` `role`

### &nbsp; Crear usuario

- Para crear un nuevo usuario, enviar una petición POST con la información del nuevo usuario en el cuerpo.

```json
{
	    "username": "",
        "name": "", 
        "email": "", 
        "password": "",
        "country": "",
        "city": "",
        "bday": "",
        "role": 0
}
```

### &nbsp; Modificar usuario
- Para modificar las propiedades de un usuario existente, enviar una petición POST indicando el nombre de usuario y los datos a modificar.

```json
{
	"username" : "",
	"method":"",
	"update":{
       colocar aquí las propiedades a actualizar con sus valores
     }
}
```

### &nbsp; Eliminar Usuario
- Para eliminar un usuario del sistema, enviar una petición DELETE a `localhost:3000/user/:username`

.

## Productos

El punto de acceso a la api de usuarios es:

```bash
localhost:3000/products
```

### &nbsp; Obtener productos

- Para obtener una lista de todos los productos, enviar una petición GET con el siguiente cuerpo: 
`{}`
- Para obtener un productos por alguna de sus propiedades, enviar una petición GET con la propiedad a filtrar y su valor en el cuerpo. 
Ej: si se quiere encontrar un producto perteneciente al usuario `van.gogh`, incluir en el cuerpo de la petición `{'username':'van.gogh'}`. Funciona para las siguientes propiedades de producto: `productid` `title` `username`
- Para obtener productos por categoría, enviar una petición GET a `localhost:3000/products/categories`con las categorías a buscar en el cuerpo. Ej: Para encontrar productos con las etiquetas `oleo` y `retrato`, el cuerpo de la petición sería:
```json
["oleo", "retrato"]
```

### &nbsp; Crear producto

- Para crear un nuevo producto, enviar una petición POST con la información del nuevo producto en el cuerpo.

```json
{
		"title":"",
		"description":"",
		"username":"",
		"images":[imgurl1, imgurl2, imgurl3],
		"tags":[]
}
```

### &nbsp; Modificar producto
- Para modificar las propiedades de un producto existente, enviar una petición POST indicando el id del producto y los datos a modificar. Incluir el campo "method" para indicar a la api que se realizará una actualización.

```json
{
	"productid" : 000,
	"method": "put",
	"update":{
       colocar aquí las propiedades a actualizar con sus valores
     }
}
```

### &nbsp; Eliminar Usuario
- Para eliminar un usuario del sistema, enviar una petición DELETE a `localhost:3000/user/:username`

## License
[MIT](https://choosealicense.com/licenses/mit/)