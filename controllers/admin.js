import userController from './user.js'

export default class Admin {
    static async userIndex (req, res) {
        try {
            let promise = await userController.getUser({})
            res.render('crud_index', { title: 'Crud usuarios', users:promise})
        } catch (err) {
            res.status(500).send(err)
        }
    }

    static async userInfo (req, res) {
        try {
            let promise = await userController.getUser(req.body)
            res.render('view', {user:promise[0]})
            
        } catch (err) {
            
        }
    }

    static async userEdit (req, res) {
        try {
            let promise = await userController.getUser(req.body)
            console.log(promise)
            res.render('edit', {title:'Editar usuario', user:promise[0]})
        } catch (err) {
            res.status(500).send(err)
        }
    }
}

