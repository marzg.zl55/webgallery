
import userController from './user.js'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'

export default class authController{

    static async signup (req, res) {
        try {
            let response = await userController.createUser(req.body)
            res.status(200).send("Usuario creado")
        } catch (err) {
            res.status(500).send("Error creando usuario")
        }
    }

    static async signin (req, res) {
        console.log(req.body)
        try {
            
            let user = await userController.getUser1({username: req.body.username})
            
            if (!user.username) throw new Error("Usuario no encontrado")
            if (!(req.body.password == user.password)) throw new Error("Contraseña inválida")
            let token = jwt.sign({username: user.username, role:user.role}, "donas", {expiresIn: 86400})
            res.status(200).send({
                username: user.username,
                email: user.email,
                accessToken: token
            })
        } catch (err) {
            res.status(500).json(err.message)
        }
    }

    static async verifyToken (req, res, next) {
        let token = req.headers["x-access-token"];
      
        if (!token) {
          return res.status(403).send({
            message: "No token provided!"
          });
        }
      
        jwt.verify(token, "donas", (err, decoded) => {
          if (err) {
            return res.status(401).send({
              message: "Unauthorized!"
            });
          }
          req.username = decoded.username
          req.role = decoded.role
          next();
        });
      };

    static async isAdmin (req, res, next) {
        if(req.role==0){
            next()
            return
        }
        res.status(403).send("No autorizado")
    }

}