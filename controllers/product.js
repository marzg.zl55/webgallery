import Product from '../models/product.js'

export default class ProductController{
    static async httpAddProduct(req, res){
        if(req.body.method){
            await this.modifyProduct(req.body)
            res.status(200).send("Producto modificado")
        }else{ 
            try {
                if(!req.body.title) throw new Error("Datos invalidos")
                let promise = await this.createProduct(req.body)
                res.status(200).send(promise)
            } catch (err) {
                console.log(err.stack)
                res.status(500).send(err.message)
            }
        }
    }
    static async httpDeleteProduct(req, res){
        let {productid} = req.params
        if (productid){
            let promise = await Product.deleteOne({productid:productid}).exec()
            res.status(200).send("Product deleted")
        }else res.status(500).send("Invalid")
    }
    static async httpGetProduct(req, res){
        try{
            let response = await Product.find(req.body).exec()
            res.status(200).send(response)
        }catch(err){
            res.status(500).send(err)
        }
    }

    static async httpAddReview(req, res){
        await this.addReview(res, req.body.productid, req.body.score)
    }

    static async httpGetProductsByTags(req, res){
        try {
            if(!req.body[0]) throw new Error("Datos inválidos")
            let response = await Product.find({tags: {$all: req.body}}).exec()
            res.status(200).send(response)
        } catch (err) {
            res.status(500).send(err.message)
        }
    }

    static async getProduct(req){
        try{
            let response = await Product.find(req).exec()
            return response
        }catch (err){
            return err
        }
    }


    static async getProductById(productid){
        try{
            let response = await Product.findOne({productid:productid}).exec()
            return response
        }catch (err){
            return err
        }
    }

    static async listProducts (req, res) {
        try{
            let promise = await Product.find(req.body).exec()
            if(!promise) res.status(404).send("Not found")
            else res.status(200).json(promise)
        }catch(err){
            res.status(500).send(err)
        }
    }

    static async createProduct (info){
        try {
            let response = await Product.create(
                {
                    productid: Math.floor(1000 + Math.random() * 9000),
                    title: info.title,
                    description: info.description,
                    valoration: 0,
                    username: info.username,
                    price:info.price,
                    images: info.images,
                    tags: info.tags,
                    date_added: new Date(),
                    state: 1
                }
            )
            return response
        } catch (err) {
            
        }
    }

    static async addReview(res, productid, score){
        try {
            let product = await this.getProductById(productid)
            if(!product) throw new Error("Producto no existe")
            if(product.valoration<=0){
                await this.modifyProduct({productid:product.productid, update:{
                    valoration: (product.valoration + score)
                }})
            }else{
                await this.modifyProduct({productid:product.productid, update:{
                    valoration: (product.valoration + score)/2
                }})
            }
            res.status(200).send("Valoración agregada")
        } catch (err) {
            res.status(500).send(err.stack)
        }
    }

    static async modifyProduct(req){
        try {
            let response = await Product.updateOne({productid:req.productid}, req.update)
            return response
        } catch (err) {
            return err
        }
    }
}
