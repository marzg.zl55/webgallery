import Transaction from '../models/transaction.js'
import productController from './product.js'
import userController from './user.js'

export default class TransactionController {

    static async createTransaction(params){
        try {
            return Transaction.create({
                id: params.id,
                productid: params.productid,
                buyer: params.buyer,
                total: params.total,
                date: params.date,
                fee: params.fee,
                payment_state:params.payment_state,
                shipping_state: params.shipping_state,
                shipping_code: params.shipping_code,
                shipping_company: params.shipping_company
            })
        } catch (err) {
            console.log(err)
            return err
        }
    }

    static async httpGetTransaction(req, res){
        try {
            const response = await this.getTransaction(req.body)
            res.status(200).json(response)
        } catch (err) {
            res.status(500).send(err.message)
        }
    }

    static async httpGetTransactionByUserCompra(req, res){
        try {
            const response = await this.getTransaction({buyer: req.params.username})
        } catch (err) {
            
        }
    }

    static async httpDeleteTransaction(req, res){
        try {
            if(!req.params.id) throw new Error("Invalido")
            const transaction = await this.getTransactionByID(req.params.id)
            const product = await productController.getProductById(transaction.productid)
            if(!transaction.id) throw new Error("Transacción no existe")
            userController.add(transaction.buyer, transaction.total)
            userController.pay(product.username, transaction.total*0.9)
            const response = await this.modifyTransaction({id:req.params.id, update:{
                payment_state: 2
            }})
            res.status(200).send("Transacción eliminada")
        } catch (err) {
            res.status(500).send(err.message)
        }
    }

    static async httpNewTransaction(req, res){
        try {
            if(!req.body.productid) throw new Error("Invalido")
            let product = await productController.getProductById(req.body.productid)
            if (!product) throw new Error("No existe el producto")
            if(product.state == 0) throw new Error("Producto no disponible")
            let sellerExists = await userController.userExists(product.username)
            let buyerExists = await userController.userExists(req.body.buyer)
            if(!sellerExists || !buyerExists) throw new Error("Usuarios invalidos")
            let fondos = await userController.fondosSuficientes(req.body.buyer, product.price)
            if(!fondos) throw new Error("Fondos insuficientes")
            console.log(product)
            let fee = product.price * 0.1
            console.log(fee)
            let response = await this.createTransaction({
                id: Math.floor(1000 + Math.random() * 9000),
                productid: product.productid,
                buyer: req.body.buyer,
                total: product.price,
                date: new Date(),
                fee: fee,
                payment_state:1,
                shipping_state: 0,
                shipping_code: "",
                shipping_company:""
            })
            await productController.modifyProduct({productid: product.productid, update:{
                state: 0
            }})
            
            await userController.pay(req.body.buyer, product.price)
            await userController.add(product.username, product.price * 0.9)
            res.status(200).send("Transacción exitosa")
        } catch (err) {
            res.status(500).send(err.message)
        }
    }

    static async modifyTransaction(params){
        try {
            let response = await Transaction.updateOne({id:params.id}, params.update)
            return response
        } catch (err) {
            return err
        }
    }

    static async deleteTransaction(id){
        try {
            let response = await Transaction.deleteOne({id:id}).exec()
            return ("Deleted")
        } catch (err) {
            console.log(err)
            return err
        }
    }

    static async getTransaction(params){
        try {
            let response = await Transaction.find(params).exec()
            return response
        } catch (err) {
            console.log(err)
            return err
        }
    }

    static async getTransactionByID(params){
        try {
            let response = await Transaction.findOne({id:params}).exec()
            return response
        } catch (err) {
            console.log(err)
            return err
        }
    }
}