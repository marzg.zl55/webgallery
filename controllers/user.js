import User from '../models/user.js'

export default class UserController {
    static async index(req, res){
        try {
            const username = req.params.username
            const user = await this.getUser(username)
            if (user) res.render('index', {title: user.name})
            else throw new Error()
        } catch (err) {
            res.status(404).render('error', {message:"error", error:{status:404, stack:{}}})
        }
    }
    static async createUser(user){
        try{
            let userExists = await this.getUser1({username:user.username})
            if (userExists){
                if(userExists.username) throw new Error("Usuario ya existe")
            }
            const response = User.create({
                username: user.username,
                name:user.name, 
                email:user.email, 
                password:user.password,
                image: "",
                country: user.country,
                city: user.city,
                rep: 0,
                bday: user.bday,
                date_added: new Date(),
                role: 1,
                level: 0,
                wallet: 0
            })
            return response
        }catch(err){
            return err
        }
    }
    static async getUser(params){
        try {
            const response = await User.find(params).exec()
            if(response[0].username) return response
            else throw new Error("Usuario no encontrado")
        } catch (err) {
            console.log(err.message)
            return err
        }
    }
    static async getUser1(params){
        try {
            
            const response = await User.findOne(params).exec()
            if(response) return response
            else throw new Error("Usuario no encontrado")
        } catch (err) {
            console.log(err)
            return err
        }
    }
    static async modifyUser(params){
        try {
            const response = await User.updateOne({username:params.username}, params.update).exec()
            return response
        } catch (err) {
            return err
        }
    }

    static async addRep(username, rep){
        try {
            const user = await this.getUser1({username:username})
            if(!user) throw new Error("Usuario no encontrado")
            console.log("ye")
            const response = await this.modifyUser({
                username:user.username,
                update : {
                    rep: user.rep + rep,
                    level: user.level + 1
                }
            })
            return response

        } catch (err) {
            console.log(err.stack)
            return err
        }
    }

    static async pay(username, price){
        const user = await this.getUser1({username:username})
        await this.modifyUser({username: username, update:{
            wallet: user.wallet - price
        }})
    }

    static async add(username, price){
        const user = await this.getUser1({username:username})
        await this.modifyUser({username: username, update:{
            wallet: user.wallet + price
        }})
    }

    static async userExists(username){
        try {
            let user = await this.getUser1({username:username})
            if(!user.username) return false
            else return true
        } catch (err) {
            return err
        }
    }

    static async fondosSuficientes(username, price){
        try {
            let user = await this.getUser1({username:username})
            return (user.wallet >= price)
        } catch (err) {
            return false
        }
    }


    //API REST
    static async httpCreateUser(req, res){
        try {
            if(req.body.method) {
                const response = await this.modifyUser(req.body)
                res.status(200).send("Usuario modificado")
                return
            }
            if(req.body.username) await this.createUser(req.body)
            else throw new Error("Datos inválidos")
            res.status(200).send("Usuario(s) Agregado(s)")
        }catch(err) {
            res.status(500).send(err.message)
        }
    }
    static async httpGetUsers(req, res){
        try {
            const response = await User.find(req.body).exec()
            res.status(200).json(response)
        } catch (err) {
            res.status(500).send(err.message)
        }
    }

    static async httpGetUser(req, res){
        try {
            if (!req.params.username) throw new Error("Invalido")
            const response = await this.getUser1({username:req.params.username})
            if(response) res.status(200).json(response)
            else throw new Error("Usuario no encontrado")
        } catch (err) {
            res.status(500).send(err)
        }
    }

    static async httpDeleteUser(req, res){
        try {
            const response = await User.deleteOne({username: req.params.username})
            res.status(200).send("Usuario eliminado")
        } catch (err) {
            res.status(500).send(err)
        }
    }
}
