import Review from '../models/review.js'

export default class ReviewController{
    static async httpGetReviews(req, res){
        try{
            let response = await Review.find(req.body).exec()
            res.status(200).send(response)
        }catch(err){
            res.status(500).send(err)
        }
    }

    static async httpAddReview(req, res){
        if(req.body.method ="put"){
            let response = await this.modifyReview(req)
            res.status(200).send("Review agregada")
        }
        else{
            let status = 200
            let promises = []
            for(i in req.body){
                try {
                    let promise = await this.createReview(req.body[i])
                    promises.push(promise)
                } catch (err) {
                    status = 500
                    promises.push(err.code)
                }
            }
            res.status(status).send(promises)
        }
    }

    static async httpDeleteReview(req, res){
        let {id} = req.params
        if (id){
            let promise = await Review.deleteOne({id:id}).exec()
            res.status(200).send("Review eliminada")
        }else res.status(500).send("Invalido")
    }

    static async modifyReview(info){
        try {
            let response = await Review.updateOne({id:info.id}, info.update)
            return response
        } catch (err) {
            return err
        }
    }

    static async createReview(info){
        return await Review.create({
            id:info.id,
            username: info.username,
            productid: info.productid,
            valoration: info.valoration,
            title: info.title,
            body: info.body
        })
    }

}