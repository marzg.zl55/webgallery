import express from 'express'
import authController from '../controllers/auth.js'

const router = express.Router()


/* GET home page. */
router.post('/signup', (req, res) => authController.signup(req, res))
router.post('/signin', (req, res) => authController.signin(req, res))


export default router