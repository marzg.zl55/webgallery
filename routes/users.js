import userController from '../controllers/user.js'
import authController from '../controllers/auth.js'
import express from 'express'
const router = express.Router();

//API
router.get('/', (req, res) => userController.httpGetUsers(req, res))
router.get('/:username', (req, res) => userController.httpGetUser(req, res))
router.post('/', (req, res) => userController.httpCreateUser(req, res))
router.delete('/:username', userController.httpDeleteUser)


export default router
