import express from 'express'
import productController from '../controllers/product.js'
var router = express.Router();

/* GET home page. */
router.get('/', (req, res) => productController.listProducts(req, res))
router.post('/', (req, res) => productController.httpAddProduct(req, res))
router.post('/review', (req, res) => productController.httpAddReview(req, res))
router.delete('/:productid', (req, res) => productController.httpDeleteProduct(req, res))
router.get('/categories', (req, res) => productController.httpGetProductsByTags(req, res))


export default router;
