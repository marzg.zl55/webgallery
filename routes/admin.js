import express from 'express'
import adminController from '../controllers/admin.js'
const router = express.Router()


/* GET home page. */
router.get('/users', (req, res) => adminController.userIndex(req, res))
router.get('/users/create', function(req, res, next) {
    res.render('create', { title: 'Crear usuario' })
})
router.get('/users/edit/:username', (req, res) => adminController.userEdit(req, res))
router.get('/users/:username', (req, res) => adminController.userInfo(req, res))


export default router