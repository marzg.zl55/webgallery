import transactionController from '../controllers/transaction.js'
import authController from '../controllers/auth.js'
import express from 'express'
const router = express.Router();

//API
router.get('/', (req, res) => transactionController.httpGetTransaction(req, res))
router.post('/', (req, res) => transactionController.httpNewTransaction(req, res))
router.delete('/:id', (req, res) => transactionController.httpDeleteTransaction(req, res))


export default router